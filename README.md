---
author: Alice Ricci
title: Entzheim
project: paged.js sprint
book format: letter
book orientation: portrait
---



## Typefaces
### Open Sans
Designed by Steve Matteson
https://fonts.google.com/specimen/Open+Sans
Apache License, Version 2.0.


##Requirements
Requires content opener image

## Supported tags
See issue
